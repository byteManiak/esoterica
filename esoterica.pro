TEMPLATE = lib
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

linux {
	LIBS += -lX11 -lglfw3 -lvulkan -ldl
}

win32 {
	LIBS += -lGLFW3 -lgdi32 "./vulkan-1.dll"
}

HEADERS += \
	es_init.h \
    es_enums.h \
    es_debug.h

SOURCES += \
	es_init.cpp

