#ifndef ES_H
#define ES_H

#include <vector>
#include <stdio.h>
#include <assert.h>
#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#define GLFW_INCLUDE_VULKAN

/* GLOBAL DEFS */
#define ES_VK_USE_VALIDATION_LAYERS

static GLFWwindow *window;
static VkInstance instance;
static VkDevice device;

#endif // ES_H
