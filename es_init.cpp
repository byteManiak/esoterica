#include "es_init.h"

static void esInitGLFW()
{
	ES_ASSERT(glfwInit(), "Could not initialise GLFW");
	ES_ASSERT(glfwVulkanSupported(), "GLFW does not support Vulkan");
	window = glfwCreateWindow(1024, 768, "Esoterica App", nullptr, nullptr);
	ES_ASSERT(window, "Could not create GLFW window");
}

static void esCreateVkInstance()
{
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.apiVersion = VK_MAKE_VERSION(1, 1, 82);

	appInfo.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
	// NOTE: Is VK application version irrelevant?
	appInfo.pApplicationName = "Esoterica App";

	appInfo.engineVersion = VK_MAKE_VERSION(0, 0, 1);
	// TODO: Make the engine version "dynamic" with maybe a bunch of defines somewhere
	appInfo.pEngineName = "Esoterica";

	VkInstanceCreateInfo instInfo = {};
	instInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instInfo.pApplicationInfo = &appInfo;

#if defined(ES_VK_USE_VALIDATION_LAYERS)
	instInfo.enabledLayerCount = 3;
	const char *validationLayers[] = {"VK_LAYER_LUNARG_core_validation",
									  "VK_LAYER_LUNARG_parameter_validation",
									  "VK_LAYER_LUNARG_standard_validation"};
	instInfo.ppEnabledLayerNames = validationLayers;
	ES_WARN("Validation layers are enabled. This should be disabled in release versions.");
#endif

	uint32_t glfwExtCount;
	glfwGetRequiredInstanceExtensions(&glfwExtCount);
	if(glfwExtCount)
	{
		const char **glfwExts = glfwGetRequiredInstanceExtensions(&glfwExtCount);
		instInfo.enabledExtensionCount = glfwExtCount;
		instInfo.ppEnabledExtensionNames = glfwExts;
	}

	VkResult result = vkCreateInstance(&instInfo, nullptr, &instance);
	ES_ASSERT((result == VK_SUCCESS), "Could not create Vulkan instance");
}

static VkPhysicalDevice pickBestPhysicalDevice(const vector<VkPhysicalDevice> &physDevices)
{
	vector<uint32_t> physDevScores(physDevices.size());
	for(uint32_t i = 0; i < physDevices.size(); i++)
	{
		VkPhysicalDeviceFeatures physDevFeatures;
		VkPhysicalDeviceProperties physDevProps;
		vkGetPhysicalDeviceFeatures(physDevices[i], &physDevFeatures);
		vkGetPhysicalDeviceProperties(physDevices[i], &physDevProps);

		switch(physDevProps.deviceType)
		{
			case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
				physDevScores[i] += 10000;
				break;
			case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
				physDevScores[i] += 1000;
				break;
			default:
				break;
		} /** Dedicated GPUs are preferred, with integrated GPUs being next. Ignore CPU/virtual GPU */
	}
	// TODO: Extend physical device picking functionality

	VkPhysicalDevice bestDevice = physDevices[0];

	uint32_t physDevScoreMax = 0;
	for(uint32_t i = 0; i < physDevices.size(); i++)
		if(physDevScores[i] > physDevScoreMax)
		{
			physDevScoreMax = physDevScores[i];
			bestDevice = physDevices[i];
		}
	/** Best physical device will have the highest score */

	return bestDevice;
}

static uint32_t pickBestQueueFamilyIndex(const vector<VkQueueFamilyProperties> queueFamilyProps)
{
	for(uint32_t i = 0; i < queueFamilyProps.size(); i++)
	{
		VkQueueFamilyProperties currentQueueFamilyProps = queueFamilyProps[i];
		if(currentQueueFamilyProps.queueCount > 0 && currentQueueFamilyProps.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			return i;
	}

	return 0;
}

static void esCreateVkDevice()
{
	uint32_t physDevCount;
	vkEnumeratePhysicalDevices(instance, &physDevCount, nullptr);
	ES_ASSERT((physDevCount > 0), "No physical devices found");

	vector<VkPhysicalDevice> physDevices(physDevCount);
	vkEnumeratePhysicalDevices(instance, &physDevCount, physDevices.data());

	VkPhysicalDevice bestDevice = pickBestPhysicalDevice(physDevices);

	uint32_t queueFamilyCount;
	vkGetPhysicalDeviceQueueFamilyProperties(bestDevice, &queueFamilyCount, nullptr);
	ES_ASSERT((queueFamilyCount > 0), "No queue families present on device");

	vector<VkQueueFamilyProperties> queueFamilyProps(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(bestDevice, &queueFamilyCount, queueFamilyProps.data());

	uint32_t bestQueueFamilyIndex = pickBestQueueFamilyIndex(queueFamilyProps);
}

void esInit()
{
	esInitGLFW();
	esCreateVkInstance();
	esCreateVkDevice();
}

void esDeinit()
{
	vkDestroyDevice(device, nullptr);
	vkDestroyInstance(instance, nullptr);
	glfwDestroyWindow(window);
}
