#ifndef ES_DEBUG
#define ES_DEBUG

#include <assert.h>
#include <stdio.h>
#include <stdarg.h>
#include "es.h"

enum ESDebugLevel
{
	ES_DEBUG_LOG, ES_DEBUG_WARN, ES_DEBUG_ERROR
};

static void __esLog(ESDebugLevel level, const char *fnName, int lineNumber, const char *format, ...)
{
	char buffer[1024];
	va_list vlist;
	va_start(vlist, format);
	vsprintf(buffer, format, vlist);
	va_end(vlist);

	switch(level)
	{
		case ES_DEBUG_LOG:
			printf("[%s():%d] %s\n", fnName, lineNumber, buffer);
			break;
		case ES_DEBUG_WARN:
			printf("[%s():%d] WARNING: %s\n", fnName, lineNumber, buffer);
			break;
		case ES_DEBUG_ERROR:
			printf("[%s():%d] ERROR: %s\n", fnName, lineNumber, buffer);
			break;
	}

}

#define _ES_LOG(level, ...) { __esLog(level, __func__, __LINE__, __VA_ARGS__); }

#define ES_LOG(...) { _ES_LOG(ES_DEBUG_LOG, __VA_ARGS__); }

#define ES_WARN(...) { _ES_LOG(ES_DEBUG_WARN, __VA_ARGS__); }

#define ES_ASSERT(_cond, ...) \
{ \
	if(!(_cond)) _ES_LOG(ES_DEBUG_ERROR, __VA_ARGS__); \
	assert(_cond); \
}

#endif // ES_DEBUG

