#include "es_window.h"

void esSetWindowMode(ESFullscreenHint fullscreenHint)
{
	// TODO: esSetWindowMode
	switch(fullscreenHint)
	{
		case ES_FULLSCREEN:
			break;
		case ES_BORDERLESS:
			glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
			break;
		case ES_WINDOWED:
			break;

		// TODO: Check if all three window-mode cases work within a DE, not a tiling WM
	}
}

void esSetWindowTitle(const char *title) { glfwSetWindowTitle(window, title); }

// TODO: Check if Vulkan context needs explicit resizing
void esSetWindowResolution(int width, int height) { glfwSetWindowSize(window, width, height); }
