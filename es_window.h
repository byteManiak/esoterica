#ifndef ES_WINDOW_H
#define ES_WINDOW_H

#include <GLFW/glfw3.h>
#include "es.h"

extern GLFWwindow *window;

enum ESFullscreenHint
{
	ES_FULLSCREEN,
	ES_WINDOWED,
	ES_BORDERLESS
};

void esSetWindowMode();
void esSetWindowTitle(const char *);
void esSetWindowResolution(int, int);

#endif // ES_WINDOW_H
